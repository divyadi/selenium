package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertFrame {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.switchTo().frame(0);
driver.findElementByXPath("//button[text()='Try it']").click();
driver.switchTo().alert().sendKeys("Divya");
driver.switchTo().alert().accept();
//Alert alert= driver.switchTo().alert();
/*WebElement web=driver.findElementByPartialLinkText("Hello Divya! How are you today?");
System.out.println(web);*/

		}

}
