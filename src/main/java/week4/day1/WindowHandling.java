package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandling {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.findElementByXPath("//span[text()='AGENT LOGIN'][1]").click();
		driver.findElementByLinkText("Contact Us").click();
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> listAllWindows=new ArrayList<>();
		listAllWindows.addAll(windowHandles);
		driver.switchTo().window(listAllWindows.get(1));
		driver.manage().window().maximize();
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dsc=new File("./Snaps/img.jpeg");
		FileUtils.copyFile(src, dsc);
		driver.switchTo().window(listAllWindows.get(0)).close();
	}

}
