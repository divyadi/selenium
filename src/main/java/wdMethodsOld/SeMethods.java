package wdMethodsOld;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class SeMethods implements WdMethods {
	public RemoteWebDriver driver;
	public int i = 1;

	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The Browser " + browser + " launched successfully");
			takeSnap();
		} catch (WebDriverException e) {
			System.err.println("WebDriverException has occured");
		} catch (NullPointerException e) {
			System.err.println("NullPointerException has occured");
		}

	}

	public WebElement locateElement(String locator, String locValue) {
		switch (locator) {
		case "id":
			return driver.findElementById(locValue);
		case "class":
			return driver.findElementByClassName(locValue);
		case "xpath":
			return driver.findElementByXPath(locValue);
		case "linktext":
			return driver.findElementByLinkText(locValue);
		case "partiallink":
			return driver.findElementByPartialLinkText(locValue);
		}
		return null;
	}

	public WebElement locateElement(String locValue) {
		return driver.findElementById(locValue);
	}

	public void type(WebElement ele, String data) {
		ele.sendKeys(data);
		System.out.println("The Data " + data + " is entered Successfully");
		takeSnap();
	}

	public void click(WebElement ele) {
		ele.click();
		System.out.println("The Element " + ele + " is clicked Successfully");
		takeSnap();
	}

	public String getText(WebElement ele) {
		ele.getText();
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		Select dropDown = new Select(ele);
		dropDown.selectByVisibleText(value);
		takeSnap();

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		Select dropIndex = new Select(ele);
		dropIndex.selectByIndex(index);
		takeSnap();

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		boolean bvalue = true;
		String title = driver.getTitle();
		if (title.equals(expectedTitle)) {
			System.out.println(bvalue);
		}
		takeSnap();
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		String text = ele.getText();
		if (text.equals(expectedText)) {
			System.out.println("Text are matched");
		} else {
			System.out.println("Text are not matched");
		}
		takeSnap();
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		String text1 = ele.getText();
		if (text1.contains(expectedText)) {
			System.out.println("Text are matched");
		} else {
			System.out.println("Text are not matched");
		}
		takeSnap();
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		Set<String> allwindows = driver.getWindowHandles();
		List<String> listofwindow = new ArrayList<>();
		listofwindow.addAll(allwindows);
		driver.switchTo().window(listofwindow.get(index));
		takeSnap();
	}

	@Override
	public void switchToFrame(WebElement ele) {
		driver.switchTo().frame(ele);
		takeSnap();
	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();

	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();

	}

	@Override
	public String getAlertText() {
		driver.switchTo().alert().getText();
		return null;
	}

	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File desc = new File("./snaps/img" + i + ".png");
			FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;

	}

	@Override
	public void closeBrowser() {
		driver.close();

	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();

	}

}
