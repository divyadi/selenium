package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import utils.ExcelObject;

public class ProjectMethods extends SeMethods{
	public static String dataSheetName;
	@BeforeSuite
	//(groups= {"common"})//
	public void beforeSuite() {
		beginResult();
	}
	@BeforeClass
	//(groups= {"common"})//
	public void beforeClass() {
		startTestCase();
	}
	//Add parameters
	@Parameters({"url",/*"username","password"*/})
	@BeforeMethod
	//(groups= {"common"})//
	public void login(String url ) {
		startApp("chrome", url);
		/*WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRM = locateElement("linkText","CRM/SFA");
		click(eleCRM);*/
	}
	@AfterMethod
	//(groups= {"common"})//
	public void closeApp() {
		closeBrowser();
	}
	@AfterSuite
	//(groups= {"common"})//
	public void afterSuite() {
		endResult();
	}

	@DataProvider(name="fetchData")
	public static Object[][] dataa() throws IOException{
	
	return	ExcelObject.getExcelData(dataSheetName);
	}
}
