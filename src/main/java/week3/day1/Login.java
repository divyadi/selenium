package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver = new ChromeDriver();
	driver.manage().window().maximize();
	try {
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	driver.get("http://leaftaps.com/opentaps");
	driver.findElementById("username1").sendKeys("DemoSalesManager");
	driver.findElementById("password").sendKeys("crmsfa");
	driver.findElementByClassName("decorativeSubmit").click();
	driver.findElementByLinkText("CRM/SFA").click();
	driver.findElementByLinkText("Create Lead").click();
	driver.findElementById("createLeadForm_companyName").sendKeys("IBM");
	driver.findElementById("createLeadForm_firstName").sendKeys("Divya");
	driver.findElementById("createLeadForm_lastName").sendKeys("S");
	//driver.findElementByClassName("smallSubmit").click();
	WebElement src=driver.findElementById("createLeadForm_dataSourceId");
	Select dropdown=new Select(src);
	WebElement src2=driver.findElementById("createLeadForm_marketingCampaignId");
	Select dropdown2=new Select(src2);
	List<WebElement> opt=dropdown2.getOptions();
	int size=opt.size();
	dropdown2.selectByIndex(size-2);
	//dropdown.selectByIndex(2);
	//dropdown.selectByValue("Cold Call");
	/*List<WebElement> options =dropdown.getOptions();
	for(WebElement eachOptions: options)
	{
	System.out.println(eachOptions.getText());
	}*/
} catch (NoSuchElementException e) {
	
	System.out.println("Exception found");
}
	finally
	{
	driver.close();
	}
	}

}

