package week5.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReport {

	public static void main(String[] args) throws IOException {
		ExtentHtmlReporter html=new ExtentHtmlReporter("./Reports/result.html");
		html.setAppendExisting(false);
		ExtentReports test=new ExtentReports();
		test.attachReporter(html);
		ExtentTest createTest = test.createTest("TC_001_Create Lead", "Create a new Lead in LeafTaps");
		createTest.assignCategory("Smoke");
		createTest.assignAuthor("Chrisdiv");
		//createTest.pass("Browser Launched Successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../Snaps/img2.png").build());
		createTest.fail("Browser not Launched Successffully", MediaEntityBuilder.createScreenCaptureFromPath("./../Snaps/img1.png").build());
createTest.pass("Username DemosalesManager Entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../Snaps/img3.png").build());
createTest.pass("Password crmsfa Entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../Snaps/img4.png").build());
		test.flush();
	}

}
