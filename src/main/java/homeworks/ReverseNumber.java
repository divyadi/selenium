package homeworks;

import java.util.Scanner;

public class ReverseNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a, rev = 0;
		System.out.println("Enter a number to reverse");
		Scanner scan = new Scanner(System.in);
		a = scan.nextInt();
		while(a != 0)
		{
			rev = rev * 10;
			rev = rev + a%10;
			a = a/10;
		}
		System.out.println("Reverse of the number is " + rev);
	}
}
