package homeworks;

import java.util.Scanner;

public class Palindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String org, rev = ""; 
	      Scanner in = new Scanner(System.in);
	 	      System.out.println("Enter the name:");
	      org = in.nextLine();
	 	      int length = org.length();
	 	      for ( int i = length - 1; i >= 0; i-- )
	         rev = rev + org.charAt(i);
	 	      if (org.equals(rev))
	         System.out.println("Entered string is a palindrome.");
	      else
	         System.out.println("Entered string isn't a palindrome.");
	  	}
}
