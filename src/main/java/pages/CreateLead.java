package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods {
public CreateLead()
{
	PageFactory.initElements(driver, this);

}
public CreateLead typeCName(String compName)
{
	WebElement cName= locateElement("id","createLeadForm_companyName");
	type(cName,compName);
	return this;
}
public CreateLead typeFName(String firstName)
{
	WebElement fName= locateElement("id","createLeadForm_firstName");
	type(fName,firstName);
	return this;
}
public CreateLead typeLName(String lasttName)
{
	WebElement lName= locateElement("id","createLeadForm_lastName");
	type(lName,lasttName);
	return this;
}
public ViewLead clickCreateLead() {
	WebElement clickLead=locateElement("class","smallSubmit");
	click(clickLead);
	return new ViewLead();
}
}
