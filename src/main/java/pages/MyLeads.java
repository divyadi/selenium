package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeads extends ProjectMethods {
	public MyLeads() {
		PageFactory.initElements(driver, this);
	}
	public CreateLead MyLeads() {
		WebElement clickMyLead= locateElement("xpath","//a[text()='Create Lead']");
		click(clickMyLead);
		return new CreateLead();
	}

}
