package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyHome extends ProjectMethods{
public MyHome() {
	PageFactory.initElements(driver, this);
	}


public MyLeads clickLeads() {
	WebElement eleClickLead=locateElement("xpath","//a[text()='Leads']");
	click(eleClickLead);
	return new MyLeads();
	
}
}

