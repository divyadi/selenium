package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLead extends ProjectMethods {
	public ViewLead()
	{
		PageFactory.initElements(driver, this);
	}
public ViewLead verifyData(String fname)
{
	
	WebElement first=locateElement("id","viewLead_firstName_sp");
	verifyExactText(first,fname);
	return this;
}
}
