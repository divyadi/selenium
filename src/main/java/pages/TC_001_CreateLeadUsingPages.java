package pages;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC_001_CreateLeadUsingPages extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName="TC002_create lead";
		testCaseDesc = "Create New Lead";
		category = "Smoke";
		author ="divya";
		dataSheetName="create Lead";
	}
	@Test(dataProvider="fetchData")
public void createL(String uName,String pswd,String compName,String firstName,String Lastname)
{
	

	
	new LoginPage().typeUserName(uName).typePassword(pswd).clickLogin().clickCrms().clickLeads().MyLeads().typeCName(compName).typeFName(firstName).typeLName(Lastname)
	.clickCreateLead().verifyData(firstName);
	
	
	}
	
}
