package testcase;

import org.testng.annotations.Test;

import utils.ExcelObject;

import org.testng.annotations.Test;
import org.testng.annotations.Test;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{
@BeforeTest
//(groups= {"smoke"})//
	public void setData() {
		testCaseName="TC002_create lead";
		testCaseDesc = "Create New Lead";
		category = "Smoke";
		author ="divya";
}
	@Test(dataProvider="fetchData")
	public void creq(String cname, String fname, String lname) {
		WebElement xpath=locateElement("xpath","//a[text()='Create Lead']");
		click(xpath);
		WebElement drop=locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingText(drop,"Cold Call");
		WebElement market=locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingIndex(market,2);
		System.out.println(market.getText());
		WebElement company=locateElement("id","createLeadForm_companyName");
		type(company,cname);
		WebElement first=locateElement("id","createLeadForm_firstName");
		type(first,fname);
		WebElement last=locateElement("id","createLeadForm_lastName");
		type(last,lname);
		WebElement clickLead=locateElement("class","smallSubmit");
		click(clickLead);
	}
	
	/*@DataProvider(name="div")
	public Object[][] data() {
		Object[][] dataa= new Object[2][3];
		dataa[0][0]="testleaf";
		dataa[0][1]="Divya";
		dataa[0][2]="S";
		dataa[1][0]="testleaf";
		dataa[1][1]="Divi";
		dataa[1][2]="S";
		return dataa;
		
	}
*/
	/*@DataProvider(name="fetchData")
	public static Object[][] dataa() throws IOException{
	
	return	ExcelObject.getExcelData();
	}
		*/
	}

