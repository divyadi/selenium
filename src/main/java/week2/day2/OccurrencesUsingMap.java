package week2.day2;

import java.util.HashMap;
import java.util.Map;

public class OccurrencesUsingMap {

	public static void main(String[] args) {
	String name="Divyaa";
	char[] ch= name.toCharArray();
	Map<Character, Integer> map=new HashMap<>();
	for(char c:ch)
	{
		if(map.containsKey(c))
		{
			Integer in=map.get(c)+1;
			map.put(c, in);
		}
		else {
			map.put(c, 1);
		}
	}
System.out.println(map);	

	}

}
