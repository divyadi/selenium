package week2.day2;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class ListOfMobUsingSet {

	public static void main(String[] args) {
		Set<String> myMobile= new LinkedHashSet<>();
		myMobile.add("Moto");
		myMobile.add("Moto");
		boolean add = myMobile.add("Nokia");
		System.out.println(add);
		myMobile.add("Vivo");
		int size = myMobile.size();		
		for(String eachMobile :myMobile )
		{
			System.out.println(eachMobile);

		}
		List<String> lst=new ArrayList<String>();
		lst.addAll(myMobile);
		String s=lst.get(7);
		System.out.println("The 1st mobile purchased:" +s);
	}

}
