package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListOfMobiles {

	public static void main(String[] args) {

		List<String> myPhones = new ArrayList<String>();
		myPhones.add("Nokia");
		myPhones.add("LG");
		myPhones.add("Oppo");
		myPhones.add("Moto");
		myPhones.add("Samsung");
		int size = myPhones.size();
		System.out.println("The count of you mobile is:" + size);
		for (String eachPhone : myPhones) {
			System.out.println(eachPhone);
		}
		System.out.println("The last mobile is:" + myPhones.get(size - 2));
		Collections.sort(myPhones); /* to sort in ASCII and print the values again use for each */
		for (String eachPhone : myPhones) {
			System.out.println(eachPhone);
		}
	}

}
